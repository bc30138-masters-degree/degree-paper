from joblib import Parallel, delayed
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import sklearn.preprocessing as preprocess

from trips_scoring.utils import NUM_CORES, all_features, \
    naming, plot_clust

low_aggressive_level = 0.1
mid_aggressive_level = 0.5
high_aggressive_level = 1.5


def get_aggressive_level(acc_value, start, end):
    aggressive_level: int
    influence = 0
    if acc_value > 0:
        if acc_value <= mid_aggressive_level:
            aggressive_level = 1
        elif acc_value > mid_aggressive_level and \
                acc_value <= high_aggressive_level:
            aggressive_level = 2
        elif acc_value > high_aggressive_level:
            aggressive_level = 3
    else:
        if acc_value >= -mid_aggressive_level:
            aggressive_level = -1
        elif acc_value < -mid_aggressive_level and \
                acc_value >= -high_aggressive_level:
            aggressive_level = -2
        elif acc_value < -high_aggressive_level:
            aggressive_level = -3

    influence = (end - start)/np.timedelta64(1, 's')

    return aggressive_level, influence


def iter_event_recognition(data_set: pd.DataFrame, feature: str, direction: str):
    event_started = False
    events_dict = {
        'event_start': [],
        'event_end': [],
        'aggressive_level': [],
        'influence': []
    }
    acc_sum: float
    acc_count: int
    for index, row in data_set.iterrows():
        if row[direction]:
            if event_started:
                acc_count += 1
                acc_sum += row[feature]
            else:
                event_started = True
                events_dict['event_start'].append(index)
                acc_count = 1
                acc_sum = row[feature]
        else:
            if event_started:
                event_started = False
                events_dict['event_end'].append(index)
                level, influence = get_aggressive_level(
                    acc_sum / acc_count,
                    events_dict['event_start'][-1],
                    index
                )
                events_dict['aggressive_level'].append(level)
                events_dict['influence'].append(influence)
            else:
                continue
    if event_started:
        events_dict['event_end'].append(
            data_set.index[-1])
        level, influence = get_aggressive_level(
            acc_sum / acc_count,
            events_dict['event_start'][-1],
            events_dict['event_end'][-1]
        )
        events_dict['aggressive_level'].append(level)
        events_dict['influence'].append(influence)

    return pd.DataFrame(events_dict)


def events_recognition(data_set):
    print('Event recognition for uid\'s {} data set with trip_id {}'.format(
        data_set['uid'], data_set['trip_id']))
    longitudinal_events: pd.DataFrame
    lateral_events: pd.DataFrame

    def acceleration_event_recog(row):
        if row > low_aggressive_level or row < -low_aggressive_level:
            return 1
        else:
            return 0

    data_set['acc_l']['longitudinal_events'] = \
        data_set['acc_l']['z'].apply(acceleration_event_recog)
    data_set['acc_l']['lateral_events'] = \
        data_set['acc_l']['x'].apply(acceleration_event_recog)

    longitudinal_events = iter_event_recognition(
        data_set['acc_l'], 'z', 'longitudinal_events')
    lateral_events = iter_event_recognition(
        data_set['acc_l'], 'x', 'lateral_events')

    trip_time = (data_set['acc_l'].index[-1] -
                 data_set['acc_l'].index[0]) / np.timedelta64(1, 's')

    return {
        'uid': data_set['uid'],
        'trip_id': data_set['trip_id'],
        'trip_time (s)': trip_time,
        "low_acceleration": longitudinal_events[
            longitudinal_events['aggressive_level'] == 1
        ]['influence'].sum() / trip_time,
        "normal_acceleration": longitudinal_events[
            longitudinal_events['aggressive_level'] == 2
        ]['influence'].sum() / trip_time,
        "aggressive_acceleration": longitudinal_events[
            longitudinal_events['aggressive_level'] == 3
        ]['influence'].sum() / trip_time,
        "low_breaking": longitudinal_events[
            longitudinal_events['aggressive_level'] == -1
        ]['influence'].sum() / trip_time,
        "normal_breaking": longitudinal_events[
            longitudinal_events['aggressive_level'] == -2
        ]['influence'].sum() / trip_time,
        "aggressive_breaking": longitudinal_events[
            longitudinal_events['aggressive_level'] == -3
        ]['influence'].sum() / trip_time,
        "low_right_move": lateral_events[
            lateral_events['aggressive_level'] == 1
        ]['influence'].sum() / trip_time,
        "normal_right_move": lateral_events[
            lateral_events['aggressive_level'] == 2
        ]['influence'].sum() / trip_time,
        "aggressive_right_move": lateral_events[
            lateral_events['aggressive_level'] == 3
        ]['influence'].sum() / trip_time,
        "low_left_move": lateral_events[
            lateral_events['aggressive_level'] == -1
        ]['influence'].sum() / trip_time,
        "normal_left_move": lateral_events[
            lateral_events['aggressive_level'] == -2
        ]['influence'].sum() / trip_time,
        "aggressive_left_move": lateral_events[
            lateral_events['aggressive_level'] == -3
        ]['influence'].sum() / trip_time
    }


def create_events_df(data_sets: list):
    events = Parallel(n_jobs=NUM_CORES)(
        delayed(events_recognition)(data_set) for data_set in data_sets)
    return pd.DataFrame(events)


def normalization(data_set):
    return pd.DataFrame(
        preprocess.minmax_scale(data_set[all_features], feature_range=(0, 1)),
        columns=all_features
    )


def kmeans_clustering(data_set, n_clust=4, norm=True):
    clust_count = {
        'features': []
    }
    for it in range(n_clust):
        clust_count['cluster {}'.format(it)] = []

    for idx_1, _ in enumerate(all_features):
        for idx_2 in range(idx_1 + 1, len(all_features)):
            features = [all_features[idx_1], all_features[idx_2]]
            res_data = data_set[features]
            if norm:
                res_data = preprocess.minmax_scale(
                    res_data, feature_range=(0, 1))
            clust_set = pd.DataFrame(res_data, columns=features)

            clust_set['cluster'] = KMeans(
                n_clusters=n_clust, random_state=0).fit_predict(clust_set)

            if norm:
                plot_clust(
                    clust_set,
                    features,
                    clust_set['cluster'].apply(
                        lambda x: "Кластер {}".format(x)),
                    plot_root='data/images/clustering/norm',
                    naming=[naming[features[0]], naming[features[1]]]
                )
                plot_clust(
                    data_set,
                    features,
                    clust_set['cluster'].apply(
                        lambda x: "Кластер {}".format(x)),
                    plot_root='data/images/clustering/result',
                    naming=[naming[features[0]], naming[features[1]]]
                )
                clust_set.to_csv(
                    'data/clustering/norm/{}_{}.csv'.format(features[0], features[1]))
            else:
                plot_clust(
                    clust_set,
                    features,
                    clust_set['cluster'].apply(
                        lambda x: "Кластер {}".format(x)),
                    plot_root='data/images/clustering/orig',
                    naming=[naming[features[0]], naming[features[1]]]
                )
                clust_set.to_csv(
                    'data/clustering/orig/{}_{}.csv'.format(features[0], features[1]))

            clust_count['features'].append("{}, {}".format(
                naming[features[0]], naming[features[1]]))
            for it in range(n_clust):
                clust_count['cluster {}'.format(it)].append(
                    clust_set[clust_set['cluster'] == it].count()['cluster'])
    if norm:
        pd.DataFrame(clust_count).to_csv('data/clustering/count_norm.csv')
    else:
        pd.DataFrame(clust_count).to_csv('data/clustering/count_orig.csv')
